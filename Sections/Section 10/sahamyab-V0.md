#### Create a Processor Group

- name it ***Sahamyab***
- click on it to get started.



### Processor 1 : GetTweets

- **name** : GetTweets

- **pre**  : -

- **type** :  InvokeHttp

- ***properties*** :

  - **Remote URL** : https://www.sahamyab.com/guest/twiter/list?v=0.1

 - ***settings***

    - **Automatically Terminate Relationships** : ALL
    
- ***scheduling***
  
  - **Run Schedule ** : 60 sec
  
  
  

#### Getting Around

- Start the pipeline 
- Right Click the processor and check the ***data provenance***
- type : ***Receive / Drop***
- Click on ***details icons*** 
- check put ***Content Tab*** & click **View**



### Processor 2 : SaveTweets

***don't start the processor --> check the back pressure*** 

 - **name** : SaveTweets

 - **pre**  : Processor 1

   - ***relation*** :  Response

 - **type** :  PutFile

 - ***properties*** :
   
     - "Directory" : "/home/nifi/workspace/tweets"
- "Conflict Resolution Strategy" : "replace"
  
  - ***settings***

    - **Automatically Terminate Relationships** : ALL

    







