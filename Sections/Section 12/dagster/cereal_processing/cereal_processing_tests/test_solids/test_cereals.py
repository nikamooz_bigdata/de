from dagster import execute_solid
from cereal_processing.pipelines.my_pipeline import MODE_TEST
from cereal_processing.solids.step_1 import hello_cereal
from cereal_processing.solids.step_2 import sort_by_calories, load_cereals
import csv, os


def test_load_cereals():

    result = execute_solid(load_cereals, mode_def=MODE_TEST)

    assert result.success
    assert len(result.output_value()) == 77


def test_sort_by_colories():

    csv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                            f"data{os.path.sep}cereal.csv")
    with open(csv_path, "r") as fd:
        cereals = [row for row in csv.DictReader(fd)]

    result = execute_solid(sort_by_calories,input_values= {"cereals" : cereals}, mode_def=MODE_TEST)
    assert result.success
    assert result.output_value()[-1]["name"] == "Strawberry Fruit Wheats"