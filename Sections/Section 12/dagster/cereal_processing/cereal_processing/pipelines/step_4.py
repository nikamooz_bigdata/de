from dagster import ModeDefinition, pipeline, PresetDefinition
from cereal_processing.solids.step_4 import read_csv, sort_by_calories_step4

MODE_DEV = ModeDefinition(name="dev", resource_defs={})
MODE_TEST = ModeDefinition(name="test", resource_defs={})


@pipeline(mode_defs=[MODE_DEV, MODE_TEST] , preset_defs=[
    PresetDefinition(
                name="Sample_Cereals_Test",
                run_config={"solids": {"read_csv": {"config": {"csv_name": "cereal_test.csv"} } } },
                mode='test'
            ),
    PresetDefinition(
                name="Sample_Cereals_Dev",
                run_config={"solids": {"read_csv": {"config": {"csv_name": "cereal.csv"} } } },
                mode='dev'
            )
    ]
)
def step4_pipeline():
    sort_by_calories_step4(read_csv())