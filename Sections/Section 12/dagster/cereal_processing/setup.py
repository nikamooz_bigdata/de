import setuptools

setuptools.setup(
    name="cereal_processing",
    packages=setuptools.find_packages(exclude=["cereal_processing_tests"]),
    install_requires=[
        "dagster==0.11.6",
        "dagit==0.11.6",
        "pytest",
    ],
)
