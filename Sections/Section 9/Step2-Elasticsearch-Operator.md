#### Airflow Custom Operator 

**we are going to transfer the result of MySQL query to Elasticsearch : ** 

- check out the `elasticsearch_operator.py` in `elasticsearch_plugin/operators`
- check out the new dag (`plugin_operator_dag.py`) in `steps/2/dags`



#### Follow These Steps : 

1- **uncomment** the `MariaDb` section in `docker-compose` file and **restart** the cluster 

2- **connect to** `MariaDB` using `DBeaver` and run the `mysql_table_sources.sql` located in `steps/2`

3- **create the connection** `mysql` and provide the credentials

- name : mysql
- ConnType : MySQL
- host : mariadb
- schema : test
- login : root
- password : root

4-**copy** `steps/2` folders to corresponding folders in airflow

5- **trigger** the `plugin_operator_dag` and inspect the logs.

6- **check** `Kibana` to ensure source data are available :

```json
GET sources/_search
{
  "query": {
    "match_all": {}
  }
}
```

or try `Discover Menu Item` after creating an index-pattern for `tweets`





