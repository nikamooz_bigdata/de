## Working With Data Frames (Parquet Files/Joins)

*source* :  [Six Spark Exercises to Rule Them All | by Andrea Ialenti | Towards Data Science](https://towardsdatascience.com/six-spark-exercises-to-rule-them-all-242445b24565)

#### Generate the data

- run `generator.py` in `Codes` folder to produce some sales data
  - Sellers
  - Products
  - Sales
- adjust number of records (`PRODUCT_NUMBERS` and `TOTAL_SALES`)
- copy `parquet` (*.parquet) files to `Docke/data/data/` folder.
- run `docker-compose up -d ` to setup the spark cluster

#### Run the hands-on Notebook

- we want to read in data with spark and explore it.
- go to `Pyspark Web UI` and open  (`step2-warm-up.ipynb`)
- run the notebook cell by cell (in order).
