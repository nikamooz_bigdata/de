# Let's Learn ksqlDB

## Introduction

[KSQL](https://ksqldb.io/) (recently rebranded ksqlDB—we'll use the two terms interchangably) is an end-to-end **event streaming platform**. In this post we'll dig into what that means and why it matters.

#### Docker Memory

Once installed, increase memory available to Docker by opening **Docker Preferences** > **Resources**, adjusting the memory slider to at least 6.0 GiB, and clicking **Apply & Restart**.

## Setup

```bash
$ cd Step3-KSQLDB
$ docker-compose up -d --build 
```

When the above completes, verify services are running:

```bash
docker-compose ps
```

## Exploring AKHQ

add these lines to your `hosts` file (*WSL/Linux/Windows*): 

- WSL : `sudo nano /etc/hosts`
- WINDOWS : open `notepad` in administrator mode , open `C:\Windows\System32\drivers\etc\hosts` (show all files)

```bash
127.0.0.1	kafka
127.0.0.1	schema-registry
127.0.0.1	connect
127.0.0.1	ksqldb-server
```

To open AKHQ, visit the following URL (may take a minute or two to initialize):

http://localhost:8080/

![](Step3-KSQLDB\img\AKHQ.png)

#### Kafka Open Source UI Tools

- Kafka-Eagle ([Kafka Eagle (kafka-eagle.org)](https://www.kafka-eagle.org/))

![Dashboard@2x.png (2874×1522) (kafka-eagle.org)](https://www.kafka-eagle.org/images/docs/Dashboard@2x.png)

- Kafkawise (https://kafkawize.com/)

![Self-service, Manage Kafka topics, acls and much more - Kafkawize](https://kafkawize.com/wp-content/uploads/2021/06/dashboardnew.jpg)

- KOWL ([cloudhut/kowl: Apache Kafka Web UI for exploring messages, consumers, configurations and more with a focus on a good UI & UX. (github.com)](https://github.com/cloudhut/kowl))

![preview](https://github.com/cloudhut/kowl/raw/master/docs/assets/preview.gif)

- Kaka Manager (Yahoo CMAK)

![topic](https://github.com/yahoo/CMAK/raw/master/img/topic.png)

## ksqlDB

Let's now dive into ksqlDB. ksqlDB is an "event streaming database purpose-built for stream processing applications" that sits on top of Apache Kafka.

But what does that mean? Let's find out.

### Architecture

<img src="https://raw.githubusercontent.com/aweagel/ksql_workshop/master/img/ksql_architecture.png" alt="KSQL Architecture" width="600" style="display: block; margin-left: auto; margin-right: auto;" />

#### KSQL Server / KSQL CLI

KSQL has a client/server architecture. The server component is internally just a generically-written JVM application leveraging the robust [Kafka Streams](https://kafka.apache.org/documentation/streams/) API. This server component exposes a REST API to which clients such as the ksqlDB CLI and the ksqlDB web UI (part of Confluent Control Center) can send queries. The ksqlDB server interprets and executes incoming SQL queries, "processing, reading, and writing data to and from the target Kafka cluster" via the Kafka Streams DSL.

KSQL provides an interactive command line interface (CLI) that can also been started via docker. In our case one such instance is already running (see `docker-compose ps`).

Let's try it out. Exec into the running `ksql-cli` service by running :

```bash
$ docker exec -it ksqldb-cli ksql http://ksqldb-server:8088
```

You should be greeted with a prompt that looks like this:

```
                  ===========================================
                  =        _  __ _____  ____  _             =
                  =       | |/ // ____|/ __ \| |            =
                  =       | ' /| (___ | |  | | |            =
                  =       |  <  \___ \| |  | | |            =
                  =       | . \ ____) | |__| | |____        =
                  =       |_|\_\_____/ \___\_\______|       =
                  =                                         =
                  =  Streaming SQL Engine for Apache Kafka® =
                  ===========================================

Copyright 2017-2021 Confluent Inc.

CLI v0.18.0, Server v0.18.0 located at http://ksqldb-server:8088
Server Status: RUNNING

Having trouble? Type 'help' (case-insensitive) for a rundown of how things work!

ksql>
```

To verify that everything is wired up appropriately, run the `SHOW TOPICS;` command. You should see output similar to the following:

```
ksql> SHOW TOPICS;

  Kafka Topic            | Partitions | Partition Replicas
----------------------------------------------------------
 docker-connect-configs | 1          | 1
 docker-connect-offsets | 25         | 1
 docker-connect-status  | 5          | 1
```

Congratulations! You've executed your first KSQL query. Although this is about the most uninteresting query we could have run, it serves its purpose of showing us how to interact with the ksqlDB server via the CLI.

Type `exit` to leave the CLI.

#### KSQL Web UI

- `kafka-eagle` (www.kafka-eagle.org) - Open Source 
- `Confluent Control Center` - 30 Days Trial 
- Trino/Presto ([Kafka connector tutorial](https://trino.io/docs/current/connector/kafka-tutorial.html))



#### Generating  Data Using Kafka Connect& Datagen Connector

Let's make things more interesting by generating some data to work with. in `config` folder, we have 2 files for generating `user` and `pageview` data using `kafka-connect`.

let see the `user` config for generating mock data :

```json
{
  "name": "datagen-users",
  "config": {
    "connector.class": "io.confluent.kafka.connect.datagen.DatagenConnector",
    "kafka.topic": "users",
    "quickstart": "users",
    "key.converter": "org.apache.kafka.connect.storage.StringConverter",
    "value.converter": "org.apache.kafka.connect.json.JsonConverter",
    "value.converter.schemas.enable": "false",
    "max.interval": 10000,
    "iterations": 100000,
    "tasks.max": "1"
  }
}
```

issue `POST` command to `kafka-connect` server for creating `user` and `pageview` connectors : 

```bash
$ curl -X POST -H "Content-Type: application/json" --data @config/connector_pageviews.config http://localhost:8083/connectors

$  curl -X POST -H "Content-Type: application/json" --data @config/connector_users.config http://localhost:8083/connectors

$ curl -s localhost:8083/connectors
["datagen-users","datagen-pageviews"]
```

- We can create/view/manage this connectors visually via `AKHQ` UI.

![](Step3-KSQLDB\img\connect.png)

**Restart/Pause/Stop**

you can use `AKHQ` Web UI to `Restart/Pause/Resume` each connector or use command line `CURL` Commands :

```bash
#Get Config
$ curl localhost:8083/connectors/datagen-pageviews
# Get Status
$ curl localhost:8083/connectors/datagen-pageviews/status
# Change Running Status
$ curl -X PUT localhost:8083/connectors/datagen-pageviews/restart
$ curl -X PUT localhost:8083/connectors/datagen-pageviews/pause
$ curl -X PUT localhost:8083/connectors/datagen-pageviews/resume
```

![](Step3-KSQLDB\img\user-topic.png)

#### Create Avro Schema 

creating Tables/Streams using Schemas is much easier than specifying every fields.  so create `users-value`  subject in Web UI/Curl using `avro` file provided in `config` folder .

`users_schema.avro` contains :

```json
{
        "namespace": "ksql",
        "name": "users",
        "type": "record",
        "fields": [
                {"name": "registertime", "type": {
                    "type": "long",
                    "arg.properties": {
                        "range": {
                            "min": 1487715775521,
                            "max": 1519273364600
                        }
                    }
                }},
                {"name": "userid", "type": {
                    "type": "string",
                    "arg.properties": {
                        "regex": "User_[1-9]{0,1}"
                    }
                }},
                {"name": "regionid", "type": {
                    "type": "string",
                    "arg.properties": {
                        "regex": "Region_[1-9]?"
                    }
                }},
                {"name": "gender", "type": {
                    "type": "string",
                    "arg.properties": {
                        "options": [
                            "MALE",
                            "FEMALE",
                            "OTHER"
                        ]
                    }
                }}
        ]
}
```

after that we have one schema to work with :

![](Step3-KSQLDB\img\Schemas.png)



### KSQL

You can inspect the messages in this topic by clicking into it within the **Topics** tab, but let's see how to do the same with KSQL.

go to `ksqldb-cli` ( Run : `docker exec -it ksqldb-cli ksql http://ksqldb-server:8088`) and issue these queries : 

```sql
PRINT 'users' FROM BEGINNING;
```

![](Step3-KSQLDB\img\user-events.png)

That's your first hint that this is a **continuous query**.

## Streams and Tables

In KSQL, there are two related concepts: **streams** and **tables**. The definitions in the Streams and Tables tabs of the Control Center web UI provide an excellent high-level introduction:

"A stream is an unbounded sequence of structured data ('facts') that you can run queries against. Streams can be created from a Kafka topic or derived from an existing stream."

"A table is a view of a stream, or another table, and represents a collection of evolving facts that you can run queries against. Tables can be created from a Kafka topic or derived from existing streams and tables."

An important note about tables is that they are only queryable when they are **materialized**:

"In ksqlDB, a table can be materialized into a view or not. If a table is created directly on top of a Kafka topic, it's not materialized. Non-materialized tables can't be queried, because they would be highly inefficient. On the other hand, if a table is derived from another collection, ksqlDB materializes its results, and you can make queries against it."

We'll work extensively with streams and tables in the steps below.

## Creating a Stream/Table
These examples query messages from Kafka topics called pageviews and users using the following schemas:

![](Step3-KSQLDB\img\ksql-quickstart-schemas.jpg)

```sql
ksql> CREATE STREAM pageviews_original (viewtime bigint, userid varchar, pageid varchar) WITH 
  (kafka_topic='pageviews', value_format='JSON');

 Message
----------------
 Stream created
----------------    

ksql> describe pageviews_original ;

Name                 : PAGEVIEWS_ORIGINAL
 Field    | Type
----------------------------
 VIEWTIME | BIGINT
 USERID   | VARCHAR(STRING)
 PAGEID   | VARCHAR(STRING)
----------------------------
For runtime statistics and query details run: DESCRIBE <Stream,Table> EXTENDED;

ksql> CREATE TABLE users_original (id VARCHAR PRIMARY KEY) WITH 
    (kafka_topic='users', value_format='AVRO');
    
 Message
---------------
 Table created
---------------
ksql> describe users_original;

Name                 : USERS_ORIGINAL
 Field        | Type
-----------------------------------------------
 ID           | VARCHAR(STRING)  (primary key)
 REGISTERTIME | BIGINT
 USERID       | VARCHAR(STRING)
 REGIONID     | VARCHAR(STRING)
 GENDER       | VARCHAR(STRING)
-----------------------------------------------
For runtime statistics and query details run: DESCRIBE <Stream,Table> EXTENDED;
```

#### Some Queries 

let's do some practical Queries : 

```sql
> show tables ;
 Table Name     | Kafka Topic | Key Format | Value Format | Windowed
---------------------------------------------------------------------
 USERS_ORIGINAL | users       | KAFKA      | AVRO         | false
 
> show streams; 
 Stream Name        | Kafka Topic | Key Format | Value Format | Windowed
-------------------------------------------------------------------------
 PAGEVIEWS_ORIGINAL | pageviews   | KAFKA      | JSON         | false
-------------------------------------------------------------------------
> select * from PAGEVIEWS_ORIGINAL;

Missing WHERE clause.  See https://cnfl.io/queries for more info.
Add EMIT CHANGES if you intended to issue a push query.
Pull queries require a WHERE clause that:
 - includes a key equality expression, e.g. `SELECT * FROM X WHERE <key-column>=Y;`.
 - in the case of a multi-column key, is a conjunction of equality expressions that cover all key columns.

If more flexible queries are needed, table scans can be enabled by setting ksql.query.pull.table.scan.enabled=true.


> select * from USERS_ORIGINAL emit changes;

```

![](Step3-KSQLDB\img\select-query.png)

```sql
 > select * from pageviews_original emit changes;
+------------------------------------------+------------------------------------------+---------------------
|VIEWTIME                                  |USERID                                    |PAGEID               
+------------------------------------------+------------------------------------------+---------------------
|1127151                                   |User_7                                    |Page_11     
|1127161                                   |User_4                                    |Page_99   
|1127171                                   |User_8                                    |Page_20   

> CREATE TABLE PAGEVIEWS_COUNT AS
SELECT pv.userid, count(*) AS view_count
FROM PAGEVIEWS_ORIGINAL pv
GROUP BY pv.userid;

 Message
-----------------------------------------------
 Created query with ID CTAS_PAGEVIEWS_COUNT_15
-----------------------------------------------

>  select * from PAGEVIEWS_COUNT  where userid='User_7';
+-----------------------------------------------------------------+------------------------------------
|USERID                                                           |VIEW_COUNT                             
+-----------------------------------------------------------------+------------------------------------
|User_7                                                           |233                                       
Query terminated
```

##### Run Query from the beginning onwards : 

Now run the following query to ***continually*** observe the changes to the `User_7` record:

```sql
> select * from PAGEVIEWS_COUNT  where userid='User_7' EMIT CHANGES ;
```

+-----------------------------------------------------------------+-----------------------------------------------------------------+
|USERID                                                           |VIEW_COUNT                                                       |
+-----------------------------------------------------------------+-----------------------------------------------------------------+
|User_7                                                           |2694                                                             |
Query terminated

##### Be Careful

``` sql
> select * from PAGEVIEWS_COUNT  where userid='User_7' EMIT CHANGES LIMIT 10;
```

+-----------------------------------------------------------------+-----------------------------------------------------------------+
|USERID                                                           |VIEW_COUNT                                                       |
+-----------------------------------------------------------------+-----------------------------------------------------------------+
|User_7                                                           |1                                                                |
|User_7                                                           |2                                                                |
|User_7                                                           |3                                                                |
|User_7                                                           |4                                                                |
|User_7                                                           |5                                                                |
|User_7                                                           |6                                                                |
|User_7                                                           |7                                                                |
|User_7                                                           |8                                                                |
|User_7                                                           |9                                                                |
|User_7                                                           |10                                                               |
Limit Reached
Query terminated

#### See Table/View Stats

```sql
> describe PAGEVIEWS_ORIGINAL extended;

Name                 : PAGEVIEWS_ORIGINAL
Type                 : STREAM
Timestamp field      : Not set - using <ROWTIME>
Key format           : KAFKA
Value format         : JSON
Kafka topic          : pageviews (partitions: 1, replication: 1)
Statement            : CREATE STREAM PAGEVIEWS_ORIGINAL (VIEWTIME BIGINT, USERID STRING, PAGEID STRING) WITH (KAFKA_TOPIC='pageviews', KEY_FORMAT='KAFKA', VALUE_FORMAT='JSON');

 Field    | Type
----------------------------
 VIEWTIME | BIGINT
 USERID   | VARCHAR(STRING)
 PAGEID   | VARCHAR(STRING)
----------------------------

Sources that have a DROP constraint on this source
--------------------------------------------------
PAGEVIEWS_COUNT

Queries that read from this STREAM
-----------------------------------
CTAS_PAGEVIEWS_COUNT_15 (RUNNING) : CREATE TABLE PAGEVIEWS_COUNT WITH (KAFKA_TOPIC='PAGEVIEWS_COUNT', PARTITIONS=1, REPLICAS=1) AS SELECT   PV.USERID USERID,   COUNT(*) VIEW_COUNT FROM PAGEVIEWS_ORIGINAL PV GROUP BY PV.USERID EMIT CHANGES;

For query topology and execution plan please run: EXPLAIN <QueryId>

Local runtime statistics
------------------------
consumer-messages-per-sec:     19.79 consumer-total-bytes:    837888 consumer-total-messages:     13092     last-message: 2021-07-16T20:54:05.789Z

(Statistics of the local KSQL server interaction with the Kafka topic pageviews)
```

#### Join Queries 

```sql
> SELECT users_original.id AS userid, pageid, regionid, gender
    FROM pageviews_original
    LEFT JOIN users_original
      ON pageviews_original.userid = users_original.id
    EMIT CHANGES
    LIMIT 5;
    
```

+-------------------------------+-------------------------------+-------------------------------+-------------------------------+
|USERID                         |PAGEID                         |REGIONID                       |GENDER                         |
+-------------------------------+-------------------------------+-------------------------------+-------------------------------+
|User_2                         |Page_25                        |Region_1                       |OTHER                          |
|User_1                         |Page_68                        |Region_2                       |OTHER                          |
|User_2                         |Page_51                        |Region_1                       |OTHER                          |
|User_5                         |Page_45                        |Region_5                       |FEMALE                         |
|User_1                         |Page_80                        |Region_2                       |OTHER                          |
Limit Reached
Query terminated     


#### Some Advanced Queries

```sql
> CREATE STREAM pageviews_enriched AS
  SELECT users_original.id AS userid, pageid, regionid, gender
  FROM pageviews_original
  LEFT JOIN users_original
    ON pageviews_original.userid = users_original.id
  EMIT CHANGES;
  
   Message
--------------------------------------------------
 Created query with ID CSAS_PAGEVIEWS_ENRICHED_17
--------------------------------------------------

> drop stream pageviews_enriched;

 Message
----------------------------------------------------------------------
 Source `PAGEVIEWS_ENRICHED` (topic: PAGEVIEWS_ENRICHED) was dropped.
----------------------------------------------------------------------

> CREATE STREAM pageviews_enriched 
  WITH (kafka_topic='pageviews_enriched') AS
  SELECT users_original.id AS userid, pageid, regionid, gender
  FROM pageviews_original
  LEFT JOIN users_original
    ON pageviews_original.userid = users_original.id
  EMIT CHANGES;
  
   Message
--------------------------------------------------
 Created query with ID CSAS_PAGEVIEWS_ENRICHED_21
--------------------------------------------------


  
```

![](Step3-KSQLDB\img\topic-consumers.png)



**A Complete Sample**

```sql
> CREATE STREAM season_length_changes 
WITH ( 
    KAFKA_TOPIC = 'season_length_changes', 
    VALUE_FORMAT = 'AVRO',
    PARTITIONS = 4,
    REPLICAS = 1
) AS SELECT 
    ROWKEY, 
    title_id, 
    IFNULL(after->season_id, before->season_id) AS season_id, 
    before->episode_count AS old_episode_count, 
    after->episode_count AS new_episode_count,
    created_at
FROM production_changes
WHERE change_type = 'season_length' 
EMIT CHANGES ;
```



#### Thumbling Window

 Tumbling time windows model fixed-size, non-overlapping, gap-less windows. A tumbling window is defined by a single property: the window’s *size*.

![](https://docs.confluent.io/platform/current/_images/streams-time-windows-tumbling.png)



Tumbling time windows are *aligned to the epoch*, with the lower interval bound being inclusive and the upper bound being exclusive. “Aligned to the epoch” means that the first window starts at timestamp zero. For example, tumbling windows with a size of 5000ms have predictable window boundaries `[0;5000),[5000;10000),...` — and **not** `[1000;6000),[6000;11000),...` or even something “random” like `[1452;6452),[6452;11452),.`

Let's create a simple tumbling window over 30 seconds interval : 

```sql
> CREATE TABLE pageviews_regions
  WITH (KEY_FORMAT='json') AS
SELECT gender, regionid , COUNT(*) AS numubers
FROM pageviews_enriched
  WINDOW TUMBLING (size 30 second)
GROUP BY gender, regionid
EMIT CHANGES;

 Message
-------------------------------------------------
 Created query with ID CTAS_PAGEVIEWS_REGIONS_27
-------------------------------------------------

>  SELECT * FROM pageviews_regions EMIT CHANGES LIMIT 5;
```

+------------------------+------------------------+------------------------+------------------------+------------------------+
|GENDER                  |REGIONID                |WINDOWSTART             |WINDOWEND               |NUMUBERS                |
+------------------------+------------------------+------------------------+------------------------+------------------------+
|MALE                    |Region_6                |1626510480000           |1626510510000           |1                       |
|MALE                    |Region_6                |1626510480000           |1626510510000           |2                       |
|OTHER                   |Region_5                |1626510480000           |1626510510000           |1                       |
|MALE                    |Region_6                |1626510480000           |1626510510000           |3                       |
|FEMALE                  |Region_2                |1626510480000           |1626510510000           |1                       |
Limit Reached
Query terminated

> Another Consumer Group Is Added!
#### A Pull Query 

```sql
> SELECT * FROM pageviews_regions WHERE gender='FEMALE' AND regionid='Region_4';

```

## Wrap Up



Let's take a step back. In this introductory tutorial, we've taken what would normally require an elaborate stream processing system such as the following:

<img src="https://raw.githubusercontent.com/aweagel/ksql_workshop/master/img/without_ksqldb.png" alt="Without ksqlDB" width="600" style="display: block; margin-left: auto; margin-right: auto;" />

and achieved the same thing with just KSQL, within a matter of minutes:

<img src="https://raw.githubusercontent.com/aweagel/ksql_workshop/master/img/with_ksqldb.png" alt="With ksqlDB" width="600" style="display: block; margin-left: auto; margin-right: auto;" />



