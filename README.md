فایلها و مستندات لازم برای اجرای عملی کارگاه‌های درس مهندسی داده موسسه آموزشی نیک آموز در این مخزن قرار گرفته است.

**فهرست مطالب**

| **Introduction to Docker**                             | **جلسه اول**        |
| ------------------------------------------------------ | ------------------- |
| **Linux Shell**                                        | **جلسه دوم**        |
| **introduction to hadoop**                             | **جلسه سوم**        |
| **Hive**                                               | **جلسه چهارم**      |
| **HBase**                                              | **جلسه پنجم**       |
| **Trino**                                              | **جلسه ششم**        |
| **Airflow-Introduction**                               | **جلسه هفتم**       |
| **Airflow- Core Concepts**                             | **جلسه هشتم**       |
| **Airflow- Advanced**                                  | **جلسه نهم**        |
| **Nifi-Introduction**                                  | **جلسه دهم**        |
| **Nifi-Advanced**                                      | **جلسه یازدهم**     |
| **ETL Modern Tools :  Dagster,Airbyte,Meltano,Singer** | **جلسه دوازدهم**    |
| **Elasticsearch-Intro**                                | **جلسه سیزدهم**     |
| **Elasticsearch-Advanced**                             | **جلسه چهاردهم**    |
| **Kafka-Intro**                                        | **جلسه پانزدهم**    |
| **Kafka-Advanced**                                     | **جلسه شانزدهم**    |
| **Spark-Intro, Install  Locally**                      | **جلسه هفدهم**      |
| **Spark Cluster-Advanced  Topics**                     | **جلسه هجدهم**      |
| **Spark Structure  Streaming**                         | **جلسه نوزدهم**     |
| **Clickhouse**                                         | **جلسه بیستم**      |
| **Apache druid**                                       | **جلسه بیست و یکم** |
| **Monitoring Tools**                                   | **جلسه بیست و دوم** |

 